<div id="services">
	<div class="row">
		<h2>OUR SERVICES</h2>
		<div class="container">
			<dl>
				<dt> <img src="public/images/content/serviceIMG1.jpg" alt="Services Image 1"> </dt>
				<dd>
					<h3>Land Clearing Service</h3>
					<a href="services#content" class="servicesbtn">LEARN MORE</a>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/serviceIMG2.jpg" alt="Services Image 1"> </dt>
				<dd>
					<h3>Residential Excavation</h3>
					<a href="services#content" class="servicesbtn">LEARN MORE</a>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/serviceIMG3.jpg" alt="Services Image 1"> </dt>
				<dd>
					<h3>Driveway Repairs</h3>
					<a href="services#content" class="servicesbtn">LEARN MORE</a>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/serviceIMG4.jpg" alt="Services Image 1"> </dt>
				<dd>
					<h3>Septic Tank Installation</h3>
					<a href="services#content" class="servicesbtn">LEARN MORE</a>
				</dd>
			</dl>
		</div>
	</div>
</div>
<div id="welcome">
	<div class="row">
		<div class="col-6 fr">
			<div class="container">
				<h3>WELCOME TO</h3>
				<h1>Magnolia Excavating</h1>
				<p>If you have used words such as ‘earthmovers near me’ while searching for a dependable land clearing service provider, you don’t need to conduct any further searches. Welcome to Magnolia Excavating, a company that provides a wide range of landscape improvement, construction, and earth moving services.</p>
				<a href="about#content" class="welcomebtn">LEARN MORE ABOUT US</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="what">
	<div class="row">
		<div class="whtLeft col-5 fl">
			<h2>WHAT WE DO</h2>
			<p>We are experts in our trade. Passion is what drives us everyday, trust us with your next project and you will experience what sets us apart. If you don’t see what you are looking for below give us a call or shoot us an email for a quote! </p>
			<ul>
				<li><span>LAND CLEARING SERVICE</span></li>
				<li><span>EARTHMOVERS NEAR ME</span></li>
				<li><span>RESIDENTIAL EXCAVATION</span></li>
				<li><span>DRIVEWAY REPAIRS</span></li>
				<li><span>SEPTIC TANK INSTALLATION</span></li>
				<li><span>HOME SITE PREP</span></li>
				<li><span>GRAVEL DRIVEWAYS</span></li>
				<li><span>GRAVEL DELIVERY</span></li>
				<li><span>BUSH HOGGING SERVICES</span></li>
				<li><span>SEPTIC TANK REPLACEMENT</span></li>
			</ul>
			<a href="service#content" class="btn">LEARN MORE</a>
		</div>
		<div class="whtRight col-7 fr">
			<div>
				<img src="public/images/content/whatIMG.jpg" alt="cat" class="whatIMG">
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="why">
	<div class="row">
		<div class="whyLeft col-6 fl">
			<div class="container">
				<h2> <span>LET US KNOW WHAT YOU NEED</span> CONTACT US</h2>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<label><span class="ctc-hide">Name</span>
						<input type="text" name="name" placeholder="Name:">
					</label>
					<label><span class="ctc-hide">Email</span>
						<input type="text" name="email" placeholder="Email:">
					</label>
					<label><span class="ctc-hide">Phone</span>
						<input type="text" name="phone" placeholder="Phone:">
					</label>
					<label><span class="ctc-hide">Message</span>
						<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
					</label>
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
					<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
				</form>
			</div>
		</div>
		<div class="whyRight col-6 fr">
			<h2>WHY CHOOSE US</h2>
			<ul>
				<li>Dedicated to Customer Satisfaction</li>
				<li>Commercial & Residential Services</li>
				<li>We are Expereinced & Knowldgable Experts</li>
				<li>Wide Selection of Quality Services</li>
			</ul>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="works">
	<div class="row">
		<h2>OUR WORKS</h2>
		<p>Our Heavy-duty wreckers get the job done with strength and efficiency.</p>
	</div>
	<div class="row">
		<div class="container clearfix">
			<div class="workLeft col-6 fl">
				<img src="public/images/content/worksIMG1.jpg" alt="Work Image 1">
				<img src="public/images/content/worksIMG2.jpg" alt="Work Image 2">
				<img src="public/images/content/worksIMG3.jpg" alt="Work Image 3">
				<img src="public/images/content/worksIMG4.jpg" alt="Work Image 4">
			</div>
			<div class="workMid col-3 fl">
				<div class="gal">
					<img src="public/images/content/worksIMG5.jpg" alt="Work Image 5">
					<div class="galleryTxt">
						<h2>GALLERY</h2>
						<a href="gallery#content" class="galleryBtn">VIEW MORE</a>
					</div>
				</div>
			</div>
			<div class="workRight col-3 fl">
				<img src="public/images/content/worksIMG6.jpg" alt="Work Image 6">
				<img src="public/images/content/worksIMG7.jpg" alt="Work Image 7">
			</div>
		</div>
	</div>
</div>
<div id="location">
	<div class="row">
		<h2>OUR LOCATION</h2>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d53462.93911012534!2d-86.89216940888859!3d33.12396903578814!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88892bc7cc017695%3A0x14dc2dea16886d1!2sMontevallo%2C+AL+35115%2C+USA!5e0!3m2!1sen!2sph!4v1540628146585" allowfullscreen></iframe>
		<h4>Primary Service Areas</h4>
		<div class="container">
			<ul>
				<li>Montevallo, AL</li>
				<li>Birmingham, AL</li>
				<li>Jemison, AL</li>
				<li>Centerville, AL</li>
				<li>Alabaster, AL</li>
			</ul>
		</div>
	</div>
</div>
